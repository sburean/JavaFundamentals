package musicPlayerV2;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.sound.midi.*;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MiniMusicAppV2{
	
	/*
	 * SEE NOTES for event handling and inner classes. 
	 */

	static JFrame f= new JFrame("My First Music Video");
	static MyDrawPanel ml;
	
	public static void main(String[] args) {
		
		MiniMusicAppV2 appV2 = new MiniMusicAppV2();
		appV2.go();
		
	}//main
	
	
	public void go(){
		guiSetup();
		
		try {
			
			//Get sequencer and open it:
			Sequencer sequencer = MidiSystem.getSequencer();
			sequencer.open();
			
			//Register for control-events with the sequencer:
			sequencer.addControllerEventListener(ml, new int[] {127}); //listen for event#127.
			
			//Create a sequence and a track:
			Sequence seq = new Sequence(Sequence.PPQ, 4);
			Track track = seq.createTrack();
			
			//Populate track with MidiEvents: (random notes between 1 to 50) - ON/OFF event-pairs.
			int rng = 0;
			for (int i = 0; i < 60; i+=4) {
				
				rng = (int) ((Math.random() * 50) + 1);
				track.add(makeMidiEvent(144, 1, rng, 100, i)); //eventON, channel 1, note i, vel 100, at timestamp i
				track.add(makeMidiEvent(176, 1, 127,   0, i)); //control-event (cmd = #176) #127 at same timestamp as eventON above. (We're listening for event #127) 
				track.add(makeMidiEvent(128, 1, rng, 100, i+2)); //eventOFF, channel 1, note i, vel 100, at timestamp i+2 (duration = 2 time units)
			}//for
			
			//Set sequence to sequencer, set tempo(bpm) and start sequencer
			sequencer.setSequence(seq);
			sequencer.setTempoInBPM(220);
			sequencer.start();
			
		} catch (MidiUnavailableException e) {
			System.err.println("Error: Could not open sequencer.");
			e.printStackTrace();
		} catch (InvalidMidiDataException e) {
			System.err.println("Error: Could not create sequence.");
			e.printStackTrace();
		}//try/catch
		
	}
	
	public void guiSetup(){
		ml = new MyDrawPanel();
		f.setContentPane(ml);
		f.setBounds(30, 30, 300, 300);
		f.setVisible(true);
	}
	
	/**
	 * Static method to help generate various MidiEvents.
	 * @param cmd The command. 144 - EventON, 128 = EventOFF, 176 = ControllerEvent (for event-handling).
	 * @param channel The channel for the tone. (use 1)
	 * @param tone (cmd dependent) For cmd = 144/128: The tone to play/stop, values between [0:127].
	 * @param vel The "hardness" of the tone, use 100.
	 * @param tick The time-stamp when we want the cmd to occur. 
	 * @return A MidiEvent object representing the specified event at the specified time.
	 */
	public static MidiEvent makeMidiEvent(int cmd, int channel, int tone, int vel, int tick) {
		
		MidiEvent event = null; 
		
		try {
			
			ShortMessage msg = new ShortMessage();
			msg.setMessage(cmd, channel, tone, vel);
			event = new MidiEvent(msg, tick);
			
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
		}
		
		return event; //Event returned is null by default, if exception occurs. 

	}//makeMidiEvent

	/**
	 * Inner class to customize a JPanel such that it creates random shapes in response to MidiControlEvents (via event handler)
	 */
	@SuppressWarnings("serial")
	class MyDrawPanel extends JPanel implements ControllerEventListener{

		boolean msg = false; //flag to indicate when to re-paint
		
		@Override
		public void controlChange(ShortMessage event) {
			/*
			 * Event-handler for when a Sequencer has processed a Midi control-change event. (see Javax.sound.midi API)
			 * This is invoked the sequencer processes a control midi event, which we will have to explicitly add to our tracks for the same time-stamps as EventON. 
			 * (we specify control-event #127 for cmd#176(control-event type), which will do nothing; gives us an event trigger!)
			 */
			msg = true;
			repaint();
		}
		
		public void paintComponent(Graphics g){
			if(msg){ //other system calls to painComponent, but we only want it to occur on MidiControl events. (flag set by event-handler)
				
				Graphics2D g2 = (Graphics2D) g;
				
				//generate random RGB values for graphic
				int r = (int) (Math.random() * 250);
				int gr = (int) (Math.random() * 250);
				int b = (int) (Math.random() * 250);
				
				//Set color of our graphic
				g2.setColor(new Color(r,gr,b));
				
				//random graphic size and location
				int ht = (int) ((Math.random() * 120) + 10);
				int width = (int) ((Math.random() * 120) + 10);
				int x = (int) ((Math.random() * 40) + 10);
				int y = (int) ((Math.random() * 40) + 10);
				
				//fill graphic with our color
				g2.fillRect(x,y,ht, width);
				msg = false;
				
			}//if
		}//paintComponent
	}//myDrawPanel
}//class
