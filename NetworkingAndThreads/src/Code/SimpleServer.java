package Code;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class SimpleServer {

	private String[] adviceList = { "Take smaller bites", 
				"Go for the tight jeans. No they do NOT make you look fat.", 
				"One word: inappropriate", 
				"Just for today, be honest. Tell your boss what you *really* think", 
				"You might want to rethink that haircut." };
	
	public void start(){
		
		System.out.println("Server up");
		ServerSocket listenSocket = null;
		
		try {
			
			//Create listening socket on port# 50000:
			listenSocket = new ServerSocket(50000);
			
			//Accept client connections & serve them for the lifetime of the server
			while(true){
				
				Socket clientConnection = listenSocket.accept(); //BLOCKING until a client connects!
				
				//service client (send a string) - SHOULD BE A SEPARATE THREAD:
				System.out.println("Servicing client: " + clientConnection.getRemoteSocketAddress());
				OutputStreamWriter char2byte = new OutputStreamWriter(clientConnection.getOutputStream());
				BufferedWriter writer = new BufferedWriter(char2byte);
				writer.write(getAdvice());
				writer.close();
				clientConnection.close();
				//Done; wait for next client
				
			}//while
			
		} catch (IOException e) {
			//Handle exceptions
			System.err.println("ERROR: server got IOException");
		} finally {
			/*
			 * Is is necessary to close a serverSocket? Server loops infinitely so unless it
			 * has to close for some reason, don't need cleanup code? 
			 */
			try {
				listenSocket.close(); //close the server socket
				System.out.println("done");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}//start
	
	public String getAdvice(){
		
		//random INTEGER between [0:length) -> ie: array 0|1|2 = [0:3)
		int rng = (int) (Math.random() * adviceList.length); 
		return adviceList[rng];
		
	}//getAdvice
	
	public static void main(String[] args) {
		SimpleServer server = new SimpleServer();
		server.start();
	}
	
}//SimpleServer