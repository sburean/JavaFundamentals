package Code;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

public class SimpleClient {

	public void start(){
		
		try {
			
			//Connect to server:
			Socket serverConnection = new Socket("127.0.0.1", 50000);
			
//			//test code:
			System.out.printf("CLIENT: local socketAddress   = %s |%n", serverConnection.getLocalSocketAddress().toString());
			System.out.printf("CLIENT: remote socket Address = %s |%n", serverConnection.getRemoteSocketAddress().toString());
			System.out.println("--------------------------------------------------");
		
			//BEST-PRACTICE METHOD:
			/******************************************************************************************
			 * While using InputStreamReader wrapped in a BufferedReader is MUCH simpler in reading   *
			 * CHARACTERS from an inputStream, what if we want to work directly with the byte stream? *
			 * Recall that any java.io.Reader is for reading Characters, so we need to work with the  *
			 * lower-level InputStream directly if we want the raw bytes, see below: 				  *
			 ******************************************************************************************/
			
			//will write read-bytes from socket connection into a byte array: (& later convert to string)
			// (or can write to another outputStream)
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(); //dynamically growing buffer
			
			/* IMPORTANT: 
			 * Generally, when we're reading bytes (a byteStream) from a InputStream that we get from a socket,
			 * we need to place it into a buffer while we work with the data we've just read. 
			 * This is because each call to InputStream.read() reads buffer.length bytes. 
			 * [see InputStream.Read(Byte[] b) API] 
			 * 
			 *  for more general info, see: 
			 *  http://stackoverflow.com/questions/5690954/java-how-to-read-an-unknown-number-of-bytes-from-an-inputstream-socket-socke
			 */
			byte[] buffer = new byte[1024]; //specified HOW MANY BYTES InputStream.read(byte[] b) reads per call
			int totalBytesRead = -1;
			int tmp = 0; // see how many times we've read from the InputStream (depends on length of buffer)
			
			//Use only this line with a small buffer size: = new byte[10] to see that each read call reads 10 bytes
//			byteArrayOutputStream.write(buffer, 0, serverConnection.getInputStream().read(buffer));
			
			while( (totalBytesRead = serverConnection.getInputStream().read(buffer)) != -1){ //(read() returns -1 when no data available) 
				//while there's data to read from inputStream, read it into the buffer and handle it:
				
				/*
				 * APPEND exact number of bytes read [0:totalBytesRead] to the ByteArrayOutputStream
				 * using just .write(byte[] b) might overwrite previous data. Try with buffer = new byte[8]
				 */
				byteArrayOutputStream.write(buffer, 0, totalBytesRead);
//				System.out.println(buffer); 
				//Note: above prints out raw data (bytes), need to convert to chars via CharSet to read it; see below:
				tmp++;
			}
			
			/* Now we can work with the bytes we've read from the InputStream (& stored into the ByteArrayOutputStream) */
			
			//print bytes converting them to a string via UTF-8 CharSet:
			System.out.println(byteArrayOutputStream.toString("UTF-8"));
			//see java.io.ByteArrayOutputStream API; don't have to close.
			
			System.out.printf("%nTotal calls to InputStream.Read(byte[] b): %d%n", tmp);
			
			/******************************************************************************************/
			
			//Directly read characters from server, and display them:
//			InputStreamReader byte2char = new InputStreamReader(serverConnection.getInputStream());
//			BufferedReader reader = new BufferedReader(byte2char);
//			System.out.println(reader.readLine());
//			reader.close();			


			serverConnection.close();
		} catch (UnknownHostException e) {
			System.err.println("ERROR: Couldn't find host");
		} catch (IOException e) {
			System.err.println("ERROR: Client got IOException");
		} //try/catch
		
	}//start
	
	public static void main(String[] args) {
		SimpleClient client = new SimpleClient();
		client.start();
	}
	
}//SimpleClient
