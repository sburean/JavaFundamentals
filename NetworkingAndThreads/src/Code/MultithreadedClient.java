package Code;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/*
 * NOTES: Need to learn how to properly clean up resources. This is NOT the correct way to clean stuff up on exit.
 * Issues:
 * 	- Once main thread finished, reader thread is still blocked reading. If we clean up (close everything) in main thread,
 * 		then reader will get IOException because resources are missing.
 * 	- Have to somehow stop reader thread from main, and clean up everything in main.
 * 	- IMPORTANT: Have to properly shut down client such that server can detect it and display "... disconnected".
 * 		Right now, it's catching connection refused exception, which happens ONLY if the client (this) DOES NOT close the serverConnection socket and just exits.
 */

/**
 * This client connects to a server [127.0.0.1:50000], and uses multi-threading to 
 * listen when the server pushes a string so it may read it, and display it. <br>
 * Therefore it may send & receive messaged <b>concurrently</b>.
 */
public class MultithreadedClient {
	
	private static final int BYTES_TO_READ = 128; //128 is large enough for this example to read everything in one .read(..)
	private static final int DELAY_SECONDS = 1; //seconds between messages sent.
	
	private Socket serverConnection;
	
	public static void main(String[] args) {
		
		MultithreadedClient mCLient = new MultithreadedClient();
		mCLient.start();

	}//main
	
	/**
	 * Started by main after it created a server connection. It reads and write messages concurrently. 
	 */
	public void start(){
		
		try {
			serverConnection = new Socket(MultithreadedServer.IP, MultithreadedServer.PORT_NUMBER);
			System.out.println("connected to: " + serverConnection.getRemoteSocketAddress());
			
			//Start separate thread to read incoming messages from the server
			Thread readingThread = new Thread(new IncomingReader(), 
					"incomingMessageReader" + serverConnection.getLocalSocketAddress());
			readingThread.start();
	
			//Set main thread name to Client<PORT> to see the sender
			Thread.currentThread().setName("Client" + serverConnection.getLocalPort());
			String clientName = Thread.currentThread().getName();
			
			//Write 3 messages to the server, 1 second apart on main thread
			for (int i = 0; i < 3; i++) {
				writeMessage(clientName + ": " + "Hello #" + i, serverConnection.getOutputStream());
				Thread.sleep(DELAY_SECONDS * 1000);
			}
			
			// once done sending messages, terminate program (stop all threads; the listener will run indefinitely)
			System.out.println(clientName + " is done");
			System.exit(0);
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("ERROR: Couldn't connect to server!");
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}//start
	

	public void writeMessage(String message, OutputStream outputStream) throws IOException {

		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
		writer.write(message);
		writer.flush();

	}//writeMessage
	
	/**
	 * Inner class that handles reading incoming messages from the server in another thread.
	 * @author Szabi Burean
	 */
	private class IncomingReader implements Runnable{

//		private Socket serverConnection;
//		
//		public IncomingReader(Socket connection) {
//			//get a reference to the server we're connected to
//			// ** OPTIONAL as outer-class has an instance variable as reference. **
//			serverConnection = connection;
//		}
		
		public void run(){
			
			ByteArrayOutputStream buff = new ByteArrayOutputStream();
			byte[] tmpBuff = new byte[BYTES_TO_READ];
			int charsRead = -1;
						
			try {
				//read all bytes, store them into buffer
				while( (charsRead = serverConnection.getInputStream().read(tmpBuff)) != -1 ){

					/*
					 * Each received message is in the form of: "<clientIP>: <message>". 
					 * Simply print each one to System.out as they're received. 
					 */
					
					buff.write(tmpBuff, 0, charsRead);
					System.out.println(buff.toString());
					buff.reset(); //reset buffer storage after each read - update as needed in context.
										
				}//while
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}//run
		
	}//Inner-Class: IncomingReader
	
}//class
