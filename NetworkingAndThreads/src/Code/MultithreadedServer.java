package Code;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

/**
 * Multithreaded server that accepts multiple client connections and reads strings they send. <br>
 * For every string read, it pushes it to all connected clients.<br>
 * Main thread simply starts server thread.<br>
 * Each client is serviced in its own thread. 
 */
public class MultithreadedServer implements Runnable{
	
	public static final String IP = "127.0.0.1";
	public static final int PORT_NUMBER = 50000;
	private static final int SLEEP_SECONDS = 120; //2minutes
	
	private  boolean running = false;
	private ServerSocket listeningConnection = null;
	private ArrayList<BufferedWriter> clients; //writer reference for each connected client
	
	public static void main(String[] args) {
		
		MultithreadedServer server = new MultithreadedServer();
		Thread serverThread = new Thread(server, "ServerThread");
		
		System.out.println("Starting server");
		serverThread.start();
		
		try {
			Thread.sleep(SLEEP_SECONDS * 1000);	
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		server.stop();
		
	}//main
	
	
	public void run(){ // main server thread
		
		openServerSocket();
		
		while(isRunning()){ /*runs server for a specified period of time - see main()*/
			
			try {
				
				Socket clientConnection = listeningConnection.accept();
				
				//service each client connection in a separate thread:
				System.out.println("Client " + clientConnection.getRemoteSocketAddress() + " connected.");
				
				//store writer reference to the client
				BufferedWriter clientWriter = new BufferedWriter(new OutputStreamWriter(clientConnection.getOutputStream()));
				clients.add(clientWriter);
//				System.out.println(clients.size()); //see size of connected clients 
				
				//start service thread for client
				Thread clientThread = new Thread(new Service(clientConnection, clientWriter), clientConnection.getRemoteSocketAddress() + "THREAD:");
				clientThread.start();
				
			} catch (IOException e) {
				if(!isRunning()){
					/* This triggers when IOException is thrown by accept() because server shut down; exit gracefully */
					try {
//						assert clients.isEmpty(); //this would make sure no clients are connected when shutting down - not realistic, could crash. Clients handle this
						System.out.println("Server Stopped");
						listeningConnection.close();
					} catch (IOException e1) {
						System.err.println("ERROR: Failed to close serverSocket after server shut down.");
						e1.printStackTrace();
					}

					return;
				} else {
					System.err.println("ERROR: Failed to accept client connection on thread: " + Thread.currentThread().getName());
					e.printStackTrace();
				}
			}
			
		}//while
		
	}//run
	
	/**
	 * Getter to determine if the server is running.
	 */
	public boolean isRunning(){
		return this.running;
	}
	
	/**
	 * Simply opens a ServerSocket on port 50000 & assigns it to an instance variable <br>
	 * Initializes the running boolean and clients arrayList.
	 */
	public void openServerSocket(){
		try {
			this.listeningConnection = new ServerSocket(PORT_NUMBER);
			this.running = true;
			this.clients = new ArrayList<>();
		} catch (IOException e) {
			System.err.println("ERROR: Couldn't open server socket...");
			e.printStackTrace();
		}
	}//openServerSocket

	/**
	 * Synchronized over the owning object (instance object of MultithreadedServer)<br>
	 * Loops through ArrayList of connected clients and sends the received message to all. <br>
	 * Called whenever the server received a message from ANY connected client.
	 */
	public synchronized void notifyClients(String message){ //synchronized over owning object: MultithreadedServer
		
		/*
		 * Only one client-serving-thread can notify all listeners at a time
		 */
		try {
			for (BufferedWriter bufferedWriter : clients) {
//				System.out.println(message); //see what we're notifying clients with
				bufferedWriter.write(message);
				bufferedWriter.flush();
			}
		} catch (IOException e) {
			System.out.println("ERROR: Failed to notify client:");
			e.printStackTrace();
		}
		
	}
	
	private class Service implements Runnable {
		
		private Socket clientConnection; 
		private BufferedWriter writer; // writer reference for THIS client we're serving; to remove from list once done service
		
		public Service(Socket client, BufferedWriter writer) {
			this.clientConnection = client;
			this.writer = writer;
		}
		
		public void run(){
			
			try {
				//listen to client and read any data it sends (while it's connected; currently the duration of the for-loop it its start method.)
				ByteArrayOutputStream buff = new ByteArrayOutputStream();
				byte[] tmpBuff = new byte[512];
				int bytesRead = -1;
				
				while( (bytesRead = clientConnection.getInputStream().read(tmpBuff)) != -1) {
					buff.write(tmpBuff, 0, bytesRead);//write read data into a storage buffer (so we can convert it to a string and use it elsewhere if needed)
					notifyClients(buff.toString());
					buff.reset(); //reset outputStream buffer after each read
				}
				
			}  catch (SocketException e) {
//				e.printStackTrace();
				if(e.toString().contains("reset")){
					//client connection lost
					try {
						//Clean up resources for client: close outputStream, inputStream, clientSocket, and remove from ArrayList of connected clients.
						clientConnection.getInputStream().close();
						clientConnection.close();
						writer.close();
						clients.remove(writer); //remote client from list of clients once done serving it/disconnected.
						System.out.println("Client: " + clientConnection.getRemoteSocketAddress() + " disconnected.");
					} catch (IOException ee) {
						System.err.println("ERROR: Failed to close clientSocket after disconnect for: " + clientConnection.getRemoteSocketAddress());
						ee.printStackTrace();
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
						
//			try {
//				writer.close();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			clients.remove(writer); //remote client from list of clients once done serving it.
//			System.out.println("Finished serving client: " + clientConnection.getRemoteSocketAddress());
			
		}//run
		
	}//Server-InnerClass
	
	/**
	 * Shut server down & clean up resources. 
	 */
	public void stop(){
		//stop server, clean & clean up resources.
		this.running = false;
		try {
			listeningConnection.close();
		} catch (IOException e) {
			System.err.println("ERROR: Failed to close serverSocket on stop()");
			e.printStackTrace();
		}
		
	}//stop
	
} // close class