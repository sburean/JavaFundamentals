package Info;

public class Locks implements Runnable{
	  
	public static final long SLEEP_TIME = 2l;
	
	public static void main(String[] args) {

		Locks tst = new Locks();
		Thread thread = new Thread(tst);
		thread.start();
		
		go();
		
		/*
		 * Both the go() method and the block inside the static foo() method are synchronized over the same key. 
		 * Since every object has different types of keys, if we have multiple things synchronized over the same key, 
		 * 	only the thread that has the key can access ANY of those synchronized blocks / methods:
		 * 
		 * See that even with the Thread.sleep in each synchronized block, the for loops run to completion - uninterrupted (atomically)
		 * 	This is because whichever thread gets the (Locks.class) key first keeps it until it finishes the method/block it's in,
		 *  before the other thread can acquire the same key for it's own work.
		 *  
		 *  See MoreLocks.java for different key usage.
		 */
		
		
	}
	
	public static synchronized void go(){ //synchronized with the class (Locks.class) key
		System.out.println("in static method: go() - synchronized");
		for (int i = 0; i < 20; i++) {
			System.out.println("go @ " + (i+1) );
			
			synchronized (Locks.class) {
				//if a thread has the "Locks.class" key, it can also access this synchronized block.
				
//				System.out.println("in synch block");
			}
			
			try {
				Thread.sleep(SLEEP_TIME); //give up this thread's active state to another thread.
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.err.println("sleep interrupted in go");
			}
			
		}
	}
	
	public static void foo(){
		System.out.println("in static method: foo() - unsynchronized");
		synchronized (Locks.class) { //synchronized with the class (Locks.class) key
			System.out.println("in static method foo() - synchronized block");
			
			for (int i = 0; i < 20; i++) {
				System.out.println("foo @ " + (i+1) );
				try {
					Thread.sleep(SLEEP_TIME); //give up this thread's active state to another thread.
				} catch (InterruptedException e) {
					System.err.println("sleep interrupted in foo");
				}
				
			}
			
		}
	}
	
	public void run(){
		
		foo();
		
	}
	

}
