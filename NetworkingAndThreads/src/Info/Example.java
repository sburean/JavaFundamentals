package Info;


public class Example implements Runnable {

	public static void main(String[] args) {
		
		/*
		 * Create two threads running the same runnable. Each prints its name while it's the active thread. 
		 * OBVIOUSLY the order that the threads print their name is unknown! 
		 * 	-> The scheduler does not guarantee when each thread will run.
		 */
		
		Example runner = new Example();
		Thread alpha = new Thread(runner, "Alpha");
		Thread beta = new Thread(runner, "Beta");
		
		alpha.start();
		beta.start();
		
	}//main
	
	public void run(){
		for (int i = 0; i < 25; i++) {
			String threadName = Thread.currentThread().getName();
			System.out.println(threadName + " is running");
			
		}
	}//run

}//class
