package Info;

public class MoreLocks implements Runnable{
	  
	public static final long SLEEP_TIME = 2l;
	
	public static void main(String[] args) {

		MoreLocks tst = new MoreLocks();
		Thread thread = new Thread(tst);
		thread.start();
		
		
		tst.go();

	}
	
	public synchronized void go(){ //synchronized with the owning object key
		System.out.println("in method: go() - synchronized");
		for (int i = 0; i < 20; i++) {
			System.out.println("go @ " + (i+1) );
			try {
				Thread.sleep(SLEEP_TIME); //give up this thread's active state to another thread.
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.err.println("sleep interrupted in go");
			}
			
		}
	}
	
	public void foo(Object obj){
		System.out.println("in method: foo() - unsynchronized");
		synchronized (obj) { //synchronized with the passed object - passing in "this" to foo is the same key as the synchronized go method.
			System.out.println("in method foo() - synchronized block");
			
			for (int i = 0; i < 20; i++) {
				System.out.println("foo @ " + (i+1) );
				try {
					Thread.sleep(SLEEP_TIME); //give up this thread's active state to another thread.
				} catch (InterruptedException e) {
					System.err.println("sleep interrupted in foo");
				}
				
			}
			
		}
	}
	
	public void run(){
		

		foo(this);
		
	}
	

}
