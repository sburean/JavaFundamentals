package Sorting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;

public class Example {

	private ArrayList<Mountain> mountainList; //Generic type invocation; when declaring a generic type, we pass it a type-parameter. Becomes a "parameterized-type"
	
	public Example() {
		go();
	}
	
	public void go(){
		mountainList = new ArrayList<>(); //construct a new ArrayList<Mountain>, compiler infers the List type; hence the diamond: <>
		mountainList.add(new Mountain(14255, "Longs"));
		mountainList.add(new Mountain(14433, "Elbert"));
		mountainList.add(new Mountain(14156, "Maroon"));
		mountainList.add(new Mountain(14265, "Castle"));
		mountainList.add(new Mountain(14156, "Derpy"));
		 
		System.out.println("before sorting: ");
		printList1(mountainList);
		
		System.out.println("-------------");
		
		Collections.sort(mountainList, new DescendingHeightComparatorCustomCompare());
		System.out.println("after sorting:");
		printList1(mountainList);
		
		System.out.println("-------------");
		
		HashSet<Mountain> mountainHashSet = new HashSet<>(); //store Mountains in a hashSet - no duplicates (need to override equals AND hashCode in Mountain!)
		mountainHashSet.addAll(mountainList); //all all mountains from list to set
		System.out.println("before sorting hashSet: "); //hashSet takes the same order as list, list was already sorted in this case!
		printList1(mountainHashSet);
		
	}
	
	public void printList1(Collection<?> collection){
		
		/*
		 * Generic method that takes in a collection of any element and prints it
		 * Recall: Generally NO difference between Collection<?> and Collection<E>, 
		 * however, E can be used in other places IF defined in scope (generic class or method)
		 * [see printList2(..) below] 
		 */
		
		for (Object e: collection) {
			System.out.println(e.toString());
		}
//		System.out.println(list);
	}
	
	public <E> void printList2(Collection<E> collection){
		//Since E was defined in this generic method, it can be used in the loop: (notice how with <?> we're refering to Object in the loop)
		for (E e : collection) {
			System.out.println(e.toString());
		}
	}
	
	@SuppressWarnings("unused")
	private class DescendingHeightComparatorBuiltInCompare implements Comparator<Mountain> {
		
		/*
		 * Inner class to sort Mountain objects by height in DESCENDING order
		 * Uses Integer.compareTo(Integer other).
		 */
		
		@Override
		public int compare(Mountain o1, Mountain o2) {
			
			/* DESCENDING ORDER:
			 * Recall: o1 is placed BEFORE o2 if comparing the two returns -1,
			 * 			and o1 is placed AFTER o2 if the comparison returns +1.
			 * (Magnitude is irrelevant) 
			 * 
			 * So for descending order, we want o1 to be placed AFTER o2 if o1 is LESS THAN o2 in some way (up to developer):
			 * - Either implement our own logic: perform a conditional check and return an appropriate result (return 1 if o1 < o2)
			 * - Or use the built in compareTo method of a primitive wrapper and alter return value accordingly
			 * 		(recall: Integet.compareTo(..) returns -1 if o1 < o2) - have to negate result of compareTo(..)
			 * 
			 * For Mountains, one mountain (o1) is less than another (o2) if height of o1 is less than height of o2.  (comparing ints for height)
			 */
			
			Integer height1 = o1.getHeight();
			Integer height2 = o2.getHeight();
			return Math.negateExact(height1.compareTo(height2)); //1 if height1 < height2, -1 if height1 > height2, 0 otherwise
			/*
			 * when negating a number represented in 2s complement, like an int, we flip the bits and add one. 
			 * This can cause an overflow on the corner case: Integer.MIN_VALUE:
			 * 
			 * 	-> MIN_VALUE = 1000 0000..0000 -> flip and add one: 0111 1111 1111 ... +1 = 1000 0000 0000 ... = MIN_VALUE again. 
			 * "overflows on the addition b/c it carries over into the 32nd bit, the sign bit."
			 * 
			 *  (for MAX_VALUE, negating it would give us -MAX_VALUE = MIN_VALUE-1) .. work it out with the bits, it works. Make sure you know how to represent #s in 2s complement.
			 * 	-> MAX_VALUE = 0111 1111 1111.. for 31 bits, representing up to (2^31)-1. When negating, this gives us 1000 0000 ... 0001, which is -2^31-1
			 */
			
			//NOTE: Math.negateExact will throw an exception if the negation will cause an overflow. see: http://stackoverflow.com/a/25203989
			
		}
		
	}
	
	@SuppressWarnings("unused")
	private class DescendingHeightComparatorCustomCompare implements Comparator<Mountain> {
		
		/*
		 * Inner class to sort Mountain objects by height in DESCENDING order
		 * Implements custom logic.
		 */
		
		@Override
		public int compare(Mountain o1, Mountain o2) {

			/* DESCENDING ORDER:
			 * Recall: o1 is placed BEFORE o2 if comparing the two returns -1,
			 * 			and o1 is placed AFTER o2 if the comparison returns +1.
			 * (Magnitude is irrelevant) 
			 * 
			 * So for descending order, we want o1 to be placed AFTER o2 if o1 is LESS THAN o2 in some way (up to developer):
			 * - Either implement our own logic: perform a conditional check and return an appropriate result (return 1 if o1 < o2)
			 * - Or use the built in compareTo method of a primitive wrapper and alter return value accordingly
			 * 		(recall: Integet.compareTo(..) returns -1 if o1 < o2) - have to negate result of compareTo(..)
			 * 
			 * For Mountains, one mountain (o1) is less than another (o2) if height of o1 is less than height of o2.  (comparing ints for height)
			 */
			
			if(o1.getHeight() < o2.getHeight()) return 1; //o1 is after o2 since its height is less than o2's height.
			if(o1.getHeight() > o2.getHeight()) return -1; //o1 is before o2 since its height is greater than o2's height.
			
			//can throw in an assert here to ensure height equality and that nothing was missed above.
//			assert o1.equals(o2);
			
			return 0; //if this is reached, o1 has the same height as o2 - don't alter their placement
		}
		
	}

	@SuppressWarnings("unused")
	private class AscendingHeightComparatorBuiltInCompare implements Comparator<Mountain> {
		
		/*
		 * Inner class to sort Mountain objects by height in ASCENDING order
		 * Uses Integer.compareTo(Integer other).
		 */
		
		@Override
		public int compare(Mountain o1, Mountain o2) {
			
			/* ASCENDING ORDER:
			 * Recall: o1 is placed BEFORE o2 if comparing the two returns -1,
			 * 			and o1 is placed AFTER o2 if the comparison returns +1.
			 * (Magnitude is irrelevant) 
			 * 
			 * So for ascending order, we want o1 to be placed BEFORE o2 if o1 is LESS THAN o2 in some way (up to developer):
			 * - Either implement our own logic: perform a conditional check and return an appropriate result (return -1 if o1 < o2)
			 * - Or use the built in compareTo method of a primitive wrapper (comparing ints for height) and alter return value accordingly
			 * 		(recall: Integet.compareTo(..) returns -1 if o1 < o2)
			 * 
			 * For Mountains, one mountain (o1) is less than another (o2) if height of o1 is less than height of o2.  (comparing ints for height)
			 */
			
			Integer height1 = o1.getHeight();
			Integer height2 = o2.getHeight();
			return height1.compareTo(height2); //-1 if height1 < height2, 1 if height1 > height2, 0 otherwise
		}
		
	}
	
	@SuppressWarnings("unused")
	private class AscendingHeightComparatorCustomCompare implements Comparator<Mountain> {
		
		/*
		 * Inner class to sort Mountain objects by height in ASCENDING order
		 * Implements custom logic.
		 */
		
		@Override
		public int compare(Mountain o1, Mountain o2) {
			
			/* ASCENDING ORDER:
			 * Recall: o1 is placed BEFORE o2 if comparing the two returns -1,
			 * 			and o1 is placed AFTER o2 if the comparison returns +1.
			 * (Magnitude is irrelevant) 
			 * 
			 * So for ascending order, we want o1 to be placed BEFORE o2 if o1 is LESS THAN o2 in some way (up to developer):
			 * - Either implement our own logic: perform a conditional check and return an appropriate result (return -1 if o1 < o2)
			 * - Or use the built in compareTo method of a primitive wrapper (comparing ints for height) and alter return value accordingly
			 * 		(recall: Integet.compareTo(..) returns -1 if o1 < o2)
			 * 
			 * For Mountains, one mountain (o1) is less than another (o2) if height of o1 is less than height of o2.  (comparing ints for height)
			 */
			
			if(o1.getHeight() < o2.getHeight()) return -1; //o1 is before o2 since its height is less than o2's height.
			if(o1.getHeight() > o2.getHeight()) return 1; //o1 is after o2 since its height is greater than o2's height.
			
			//can throw in an assert here to ensure height equality and that nothing was missed above.
			
			return 0; //if this is reached, o1 has the same height as o2 - don't alter their placement
		}
		
	}
	
	public static void main(String[] args) {
		@SuppressWarnings("unused") //constructor starts program - testing purposes.
		Example ex = new Example();	
	}
	
}
