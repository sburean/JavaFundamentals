package Sorting;

/*
 * dummy-class for sorting. Doesn't implement Comparable<T>, therefore we must use a Comparator<T> class.
 * -> See inner class of "Example"
 */

public class Mountain {

	int height;
	String name;
	
	public Mountain(int h, String n) {
		this.height = h;
		this.name = n;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getHeight(){
		return this.height;
	}
	
	@Override
	public String toString() {
		return name + ": " + height;
	}
	
	@Override
	public int hashCode() {
		/*
		 * Override hashcode if two mountains are to be meaningfully equivalent.
		 * If two objects return the same hashCode, they are the same. Period. 
		 * -> If override equals, MUST override hashCode() as well!
		 * 
		 *  By default, objects return a hashCode based on their location in memory, therefore all objects are unique
		 *  with respect to comparisons and equality. Need to override to produce a hashCode that's appropriate to context. 
		 *  
		 * -> Once decided on the factor that makes two mountains equal (their height), simply delegate 
		 * 	  the hashCode call to the instance variables of that type (Wrappers, or String)
		 * 
		 * If two objects have the same hashCode (a.hashCode() == b.hashCode()), it DOES NOT MEAN that they are equal!
		 * (two different objects can have the same hashCode)
		 */
		
		return Integer.hashCode(this.height);
	}
	
	@Override
	public boolean equals(Object obj) {
		/*
		 * Override equals to have a way to indicate if two mountains are meaningfully equivalent.
		 * 	-> Simply decide on the factor that makes two mountains equal (their height) and 
		 * 	   then delegate the equality call on instance variables of that type (Wrappers or String):
		 * 
		 *   If two objects are equal, then a.equals(b) (same as b.equals(a)) means a.hashCode() == b.hashCode()!
		 */
		
		if(!(obj instanceof Mountain)) return false; // comparing apples and oranges, not equal
		
		Mountain otherMountain = (Mountain) obj; //safe to cast, obj must be instanceof Mountain
		
		Integer iHeight = new Integer(this.height);
	
		return iHeight.equals(otherMountain.getHeight()); //delegate equality call to integer wrapper.
		
	}
	
}
