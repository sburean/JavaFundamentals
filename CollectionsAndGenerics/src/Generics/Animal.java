package Generics;

//Class for example; see "GenericPolymorphism.java"

public class Animal {

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}
	
	public void sleep(){
		System.out.println("Animal is sleeping.");
	}
	
}
