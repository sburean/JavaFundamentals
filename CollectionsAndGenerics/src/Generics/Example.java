package Generics;

import java.util.List;

public class Example {

	public <T> void methodOne(T object){
		//erasure changed <T> into Object at the byte-code level. (since the type parameter is unbound)
	}
	
	public void methodTwo(Object object){
		
	}
	
	public void methodThree(List<?> object){
		/*
		 * This is NOT the same as List<Object>!!
		 * List<Object> works with any object that is of Type Object somewhere in the inheritance tree (ie: all objects)
		 * List<?> is a list of unknown types. We don't know what we're working with, so we can't just insert anything in the list. (tho we can still only work with Object-type methods)
		 * -> List<?> is the same as List<E>
		 * Obviously, if we use List<E>, we can use E in other places as long as it's defined in scope.. either in 
		 * a containing generic class or before the return type of the generic method. 
		 * 
		 * SEE: http://stackoverflow.com/a/28760323
		 */

	}
	
	public <T> void methodFour(List<T> list, T obj){
		list.add(obj);
	}
	
	public void methodFive(List<Object> list, Object obj){
		list.add(obj);
	}

}
