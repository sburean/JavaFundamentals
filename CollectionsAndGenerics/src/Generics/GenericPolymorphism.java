package Generics;

import java.util.ArrayList;
import java.util.List;

public class GenericPolymorphism<T> {

	public void print(T object){
		System.out.println("have a: " + object.getClass().getSimpleName());
	}
	
	public static void main(String[] args) {
		
		/*
		 * Generic class can take any object type (or as specified by the upper-bound: <? extends UpperBound> )
		 * Polymorphism applies normally to any generic method within the generic class with specified formal parameters as such:
		 * 
		 * 	ie: foo(Animal o), will only accept invocations that contain compatible types with Animal. (compatible with the formal parameter)
		 * 	This is simply normal polymorphism!
		 */
		
		GenericPolymorphism<Animal> one = new GenericPolymorphism<>();
		one.print(new Animal());
		one.print(new Cat());
		one.print(new Dog());
		
		ArrayList<Animal> two = new ArrayList<>(); 
		two.add(new Animal());
		two.add(new Cat());
		two.add(new Dog());
		
		/****************************************************************************************************************************************
		 * However, if we have a method that takes a formal-parameter with a SPECIFIED type-parameter, 											*
		 * it will only accept objects compatible with the formal-parameter, and ONLY with the specified type-parameter!!						*
		 * (meaning polymorphism works normally on the formal parameter, as in the first case, but must keep the specified type-parameter)		*
		 * 																																		*
		 * ie: foo(List<Animal> list) will only accept invocations that contain compatible types with List, AND <Animal> as the type-parameter.	*
		 * 		valid: ArrayList<Animal>, List<Animal>, any object that passes the IS-A test with list<Animal>. 								*
		 * 	  invalid: ArrayList<Dog>, List<Cat>, etc...																						*
		 ****************************************************************************************************************************************/
		
		/*
		 * KEY CONCEPT: 
		 * Generics enables greater of flexibility to our code by allowing a wider scope of inputs to our generic code. 
		 * 
		 * -> Consider polymorphism without generics, if we have a method that has a polymorphic formal parameter, 
		 * 		we can invoke that method with any argument that is compatible with the formal parameter (passes IS-A test).
		 * 		Therefore we can reuse the polymorphic method with multiple concrete types.
		 * 
		 * -> Now consider generics. If we have a method with a formal-parameter and a BOUNDED type-parameter,
		 * 		we can invoke that method with any formal-argument that is compatible with the formal-parameter, AND
		 * 		and type-argument that is compatible with the type-parameter, as specified by the UPPER/LOWER-BOUNDS. 
		 * 			--> (no bounds = no compatibility, regardless of type's inheritance relationships & we're in a non-generic polymorphic scenario) <--
		 * 
		 * 		Therefore we can reuse the generic polymorphic method as well, but with a "wider scope". 
		 * 	ie: It will accept multiple concrete types of the formal parameter, and multiple type-arguments as allowed by the upper-bound.
		 * 		(see foo & bar below)
		 */
		
		one.foo(two); //passing in an ArrayList<Animal> into foo; valid - compatible type-parameter with Animal: <A extends Animal>
		
		ArrayList<Cat> cats = new ArrayList<>();
		cats.add(new Cat());
		cats.add(new Cat());
		
		one.foo(cats); //passing in an ArrayList<Cats> into foo; valid - compatible type-parameter with Animal: <A extends Animal>

	}
	
	public <A extends Animal> void foo(List<A> list){ // "Two layers of Polymorphism" - with generics; same as: public void foo(List<? extends Animal> list){..}
		
		/*
		 * [Upper-bound on type-parameter = Animal.]
		 * 
		 * Can invoke foo(..) with any argument that is compatible with the formal-parameter (List, ArrayList, etc..),
		 * AND with any type-parameter compatible with upper-bound Animal (Animal, Dog, Cat, etc..),
		 * since it was declared with the wild-card.
		 * 
		 * VALID : (see above in main())
		 * foo(list<Animal>)
		 * foo(ArrayList<Cat>)
		 * etc...
		 */
		
		System.out.println();
		for (A type : list) {
			System.out.println(type.toString());
		}
		
	}
	
	public void bar(ArrayList<Animal> animalList){ // "One layer of Polymorphism" - without generics
		
		/*
		 * The formal parameter animalList is of a specific type. It is the same as declaring: bar(Object o)
		 * Polymorphism applies; we can invoke bar with any argument that is compatible with object.
		 * (anything that passes the IS-A test with Object) 
		 * 
		 * We CAN'T pass in an ArrayList<Cat> because it does not pass the IS-A test with ArrayList<Animal>
		 * They are BOTH on the same hierarchy level with their parent !
		 * [AbstractList(i think?) or whatever ArrayList extends..] 
		 * Point is, the type-parameter plays no effect in the IS-A test !
		 */
		
		for (Animal animal : animalList) {
			System.out.println(animal.toString());
		}
	}
	
	/*
	 * Also keep in mind that you can do this:
	 * - return a list of objects that themselves hold objects (of a certain bound)
	 * 
	 * ie: return an arrayList of Zoo objects that hold any object compatible with Animal themselves 
	 */
	
	public <G extends Animal> ArrayList<Zoo<G>> getZooList(int someParameter){
		/*
		 * NOTE: 'G' in ArrayList<Zoo<G>> could further branch out by stating any sub-types of type-G as needed:
		 * 		ArrayList<Zoo<? extends G>>
		 * So this returns an ArrayList as such: ArrayList<Zoo<Cat>>, or ArrayList<Zoo<Dog>>
		 */
		return null;
	}
	
}