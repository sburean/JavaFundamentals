/*
 * The testCode for our program. We write this first to ensure it's done, and then write the 
 * appropriate implementation for the tests. Once it passes, can add more tests, and then
 * continue to add implementations. This process is repeated until program is complete.
 */

public class SimpleBattleshipTestClass {

	public String test() {

		//Setup:
		SimpleBattleship battleShipGame = new SimpleBattleship();
		int[] locations = {2,3,4};
		battleShipGame.setLocationCells(locations);
		
		String usrGuess = "2"; //guess a value in the locations array; result should be a "hit"
		
		String tstResult = "failed";
		if(battleShipGame.checkYourself(usrGuess).equals("hit")){
			tstResult = "passed";
		}
		
		return tstResult; //return test result
		
	}

}
