/*
 * This is our "prototype" of the battleship class. 
 * It is a simplified version of the actual program.
 * It implements logic that decides if a user hit or missed this ship. 
 * Will eventually be scaled up for the final design. 
 */

public class SimpleBattleship {

	//Instance variables (have default values)
	private int[] locationCells; 
	private int numOfHits;
	
	//Setters:
	public void setLocationCells(int[] locations) {
		this.locationCells = locations;
	}

	//Methods:
	public String checkYourself(String usrGuess) {
		
		int guess = Integer.parseInt(usrGuess);
		String result = "miss";
		
		for (int cell : locationCells) {
			if(cell == guess){
				//If user guessed correctly, increment number of hits, and stop searching for a hit
				
				/*
				 * NOTE: Have to prevent the user from simply guessing an already-guessed value for successive hits!
				 * 			See "Battleship" project.
				 */
				
				result = "hit";
				numOfHits++;
				break;
			}
		}
		
		if(numOfHits == locationCells.length){
			//if all points on a ship have been hit, it is sunk
			result = "kill";
		}
		
		return result;
		
	}
	
}
