/*
 * The class that runs the actual game. 
 * Everything is done in main for this simplified version of the game.
 *  - keep track of how many times the player guessed
 *  - generate a random number, create a single battleship starting from the value with a size of 3
 *  - while the game is running, take user guesses & check if they are correct
 *  
 * The prototype version of the game contains a single "battleShip" represented by an array on a VIRTUAL row.
 * The virtual row can be thought of as a row of integers: |1|2|3|4|5|6|7|etc..
 * Our battleship is simply an array containing <size> consecutive integers of the row.
 * (example: for a battleship positioned at {3,4,5}: |1|2|x|x|x|6|7|etc..
 */

public class SimpleBattleshipGame {
	
	public static void main(String[] args) {
		
		/*
		 * NOTE: Bug in the simple version, where if you get a hit can simply repeat the same number for multiple hits.
		 * 			See the more complex "Battleship" project for full code and bug fix.
		 */
		
		//Instance variables:
		int numOfGuesses = 0;
		boolean isAlive = true; 
		
		//Setup
		SimpleGameHelper helper = new SimpleGameHelper(); //to get user input
		SimpleBattleship bShip = new SimpleBattleship();
		int rng = (int) (Math.random() * 5); //random number between 0 and 5 for starting location (because total range is 0->7 & need at least two extra spots after starting location)
		int[] locations = {rng, rng+1, rng+2};
		bShip.setLocationCells(locations);
		
		while(isAlive){
			
			//Ask for user input
			String usrIN = helper.getUsrInput("enter a number");
			numOfGuesses++;
			
			//Check if hit or miss
			String result = bShip.checkYourself(usrIN);
			System.out.println(result);
			
			//if kill, end game
			if(result.equals("kill")){
				isAlive = false;
				System.out.println("You took " + numOfGuesses + " guesses");
			}
		}//while
	}//main
}//class
