import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * A helper class that's used to get user input.
 */
public class SimpleGameHelper {
	
	public String getUsrInput(String promptMsg){
		
		System.out.print(promptMsg + " ");
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		String usrInput = null;
		
		try {
			//Catch any java.io exceptions
			usrInput = input.readLine();
			if (usrInput.length() == 0) return null; //user didn't actually type anything
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return usrInput;
		
	}

}
