package tutorial;

import javax.sound.midi.*;

/*
 * Class to test a javax.sound.midi.Sequencer object.
 */

public class MusicTest1 {
	
	public void play(){
		
		/*
		 * This is risky-behaviour. It may fail at runtime due to circumstances that are out of our control.
		 * We have to acknowledge the risks AND handle them! (or forward the exception to the next method on the call-stack)
		 * --> "CheckedExceptions" - Have to explicitly acknowledge and handle or declare.
		 */
		try{ 
			// 1: acknowledge the risk
			
			/*
			 * Get a sequencer object. It's the thing that sequences all MIDI information into a 'song'. 
			 * We can simply ask the MidiSystem to give us a song, we don't have to have one.
			 */
			Sequencer sequencer = MidiSystem.getSequencer(); //risk indicated via "throws" keyword from the "getSequencer()" method.
			System.out.println("We got a sequencer");
			
		} catch (MidiUnavailableException ex){
			// 2: handle the risk when the exceptional situation occurs (only runs if the appropriate exception is thrown)
			
//			ex.printStackTrace(); //if can't recover, at least print stack trace so others can figure out what happened. 
			System.out.println("Bummer");
		}
		
	}//play
	

	public static void main(String[] args) {
		MusicTest1 test = new MusicTest1();
		test.play();
	}//main
	
}//class
