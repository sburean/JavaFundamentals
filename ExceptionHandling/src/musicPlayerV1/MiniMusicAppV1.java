package musicPlayerV1;

import javax.sound.midi.*;

public class MiniMusicAppV1 {

public static void main(String[] args) {
		
		if(args.length < 2){
			System.out.println("Don't forget the instrument and note args");
		} else {
			/*
			 * args: [0:127]
			 * arg1 = set instrument
			 * arg2 = set note
			 */
			
			int instrument = Integer.parseInt(args[0]);
			int sound = Integer.parseInt(args[1]);
			
			MiniMusicAppV1 mini = new MiniMusicAppV1();
			mini.play(instrument, sound);
		}
		
	} // close main

	public void play(int instrument, int note){
		
		try {
			
			Sequencer sequencer = MidiSystem.getSequencer();
			sequencer.open();
			
			Sequence seq = new Sequence(Sequence.PPQ, 4);
			
			Track track = seq.createTrack();
			
			ShortMessage setInstrumentMessage = new ShortMessage();
			setInstrumentMessage.setMessage(192, 1, instrument, 100);
			MidiEvent setInstrumentEvent = new MidiEvent(setInstrumentMessage, 1);
			track.add(setInstrumentEvent);
			
			ShortMessage setNoteOnMessage = new ShortMessage();
			setNoteOnMessage.setMessage(144, 1, note, 100);
			MidiEvent setNoteOnEvent = new MidiEvent(setNoteOnMessage, 1);
			track.add(setNoteOnEvent);
			
			ShortMessage setNoteOffMessage = new ShortMessage();
			setNoteOffMessage.setMessage(128, 1, note, 100);
			MidiEvent setNoteOffEvent = new MidiEvent(setNoteOffMessage, 16);
			track.add(setNoteOffEvent);
			
			sequencer.setSequence(seq);
			
			sequencer.start();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		
	}//play
	
}//class
