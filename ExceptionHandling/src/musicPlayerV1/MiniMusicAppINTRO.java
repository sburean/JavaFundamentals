package musicPlayerV1;

import javax.sound.midi.*;

public class MiniMusicAppINTRO {

	public static void main(String[] args) {

		MiniMusicAppINTRO mini = new MiniMusicAppINTRO();
		mini.play();
		
	} // close main
	
	public void play(){
		
		
		try {
			//1: Get a Sequencer & open it	
			Sequencer sequencer = MidiSystem.getSequencer();
			sequencer.open();
			
			//2: Make a new Sequence
			Sequence seq = new Sequence(Sequence.PPQ, 4);
			
			/*
			 * 3: Get a new Track from the sequence.
			 * Remember, the track lives in the sequence, and MIDI data lives within the track.
			 * 
			 * -> Track holds all MIDI events; sequence organizes MIDI events based on 
			 * 		when they're supposed to play (sequentially), and 
			 * 		the sequencer plays them back in that order.
			 * -> We can obviously have multiple events happening at the same point in time!
			 * 		(Two different instruments playing different sounds at the same time) 
			 */
			Track track = seq.createTrack(); //
			
			/*
			 * 4: Fill the track with MIDI-Instructions & give the track to the sequencer
			 * NOTE: A track needs DETAILED instructions, therefore we need a MIDI-instruction for 
			 * 			when to start playing a note (ON), and when to stop playing a note (OFF).
			 * 
			 * -> Need both message (what) and midiEvent (when) for a MIDI-Instruction.
			 */
			ShortMessage a = new ShortMessage(); //Message says "what to do" - contains instructions.
			a.setMessage(144, 1, 44, 100); //play note '44', etc.. (the actual instructions; see below)
			MidiEvent noteOn = new MidiEvent(a, 1);//MidiEvent says "when to do it"; combination of message and time-stamp of when a ShortMessage should "fire"; (ie: trigger message 'a' on beat '1'.)
			track.add(noteOn); //ON event for specified message.
			
			ShortMessage b = new ShortMessage();
			
			/*
			 *  - Arg1 = the type of message. Values of other three args depend on arg1. (ie: message type 144 = note ON, and message type 128 = note OFF)
			 *  - Arg2 = the channel. Think of it as a musician in a band. (ie: what kind of note to play? channel 1 is the pianist, channel 2 is the drummer, etc...)
			 *  - Arg3 = (for arg1 = 144/128) the note to play. Ranges from 0 (low) to 127 (high).
			 *  		 (for arg1 = 192-changeInstrucment) the type of instrument to set for the specified channel via arg2. Specify an instrument ranging from 0-127. 
			 *  - Arg4 = the velocity of the note; how "hard" to press it. (ie: 0 is soft, while 100 is a good default) .. what's the range?
			 */
			b.setMessage(128, 1, 44, 100);
			
			MidiEvent noteOff = new MidiEvent(a, 16); //specifies the "off" time-stamp for message a. (OFF because message type = 128, therefore it MUST occur at a LATER time-stamp than the ON message above)
			track.add(noteOff); //OFF event for specified message.
			
			sequencer.setSequence(seq);
			
			//5: Start the sequencer. (push "play")
			sequencer.start();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
			
	}//play
}//class
