package Code;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/*
 * Quick test of java.io classes to write and read a file from disk.
 *  Using the File class to specify a destination)
 *  
 *  NOTE: See NetworkingAndThreads/src/SimpleClient for more in-depth IO reading (& best-practice)!
 */

public class MyClass {
	
	private static final String FILE_NAME = "io_test.txt";
	
	public static void main(String[] args) {

		/* Test using the new java.nio.file.Path class:
		 *
		 * 	//Create a path (absolute path):
		 *  Path path = Paths.get("/Szabi", "Documents", "Java", "JavaFundamentals", "IOTest", "tmp.txt");
		 *  
		 *  System.out.format("toString: %s%n", path.toString());
		 *  System.out.format("getFileName: %s%n", path.getFileName());
		 *	System.out.format("getName(0): %s%n", path.getName(0));
		 *	System.out.format("getNameCount: %d%n", path.getNameCount());
		 *	System.out.format("subpath(0,2): %s%n", path.subpath(0,2));
		 *	System.out.format("getParent: %s%n", path.getParent());
		 *	System.out.format("getRoot: %s%n", path.getRoot());
		 *
		 */

		//NOTE: writing can go in a method, simply pass in a file name to write-to.
		//Create a file object (use file.getAbsolutePath() to see where it is located)
//		File file = new File("io_test.txt");
		
		// OR: Can also create file from scratch using the system-independent file separators:
		File file = new File(File.separator + "Users" + File.separator + "Szabi" + File.separator 
				+ "Documents" + File.separator + "Java"+ File.separator + "JavaFundamentals" 
				+ File.separator + "IOTest" + File.separator + FILE_NAME);

		System.out.println("Using file: " + file + "\n");
		
		if(file.isFile() && file.canWrite()){
			//We can write to our file:
			
			try {
				
				//FileWriter creates file if doesn't exist - NOTE: FileWriter is a convenience method for writing characters to a file; skips the middle-man, see below:
//				FileWriter fw = new FileWriter(file); //writing text (characters) to file, may be read by non-java applications.
//				BufferedWriter bw = new BufferedWriter(fw); //wrap the FileWriter in a BufferedWriter for efficiency.
//				for (int i = 0; i < 5; i++) {
//					bw.write("This is line number: " + i + "\n");
//				}
//				bw.close();
				
				//WITHOUT using the "FileWriter" convenience method: --- same result as above! HOWEVER, this gives flexibility to provide own encoding char-set. [see API]
				//for more info: http://stackoverflow.com/questions/15803350/filewrite-bufferedwriter-and-printwriter-combined
				FileOutputStream bytes2file = new FileOutputStream(file);
				OutputStreamWriter chars2bytes = new OutputStreamWriter(bytes2file);
				BufferedWriter efficiency = new BufferedWriter(chars2bytes);
				for (int i = 0; i < 5; i++) {
					efficiency.write("This is line number: " + i + "\n");
				}
				efficiency.close();
				
				/*****************************WRITING***************************************
				 * OutputStreamWriter char2byte = new OutputStreamWriter( <outputStream> );
				 * BufferedWriter writer = new BufferedWriter(char2byte);
				 * writer.write("blah");
				 * 
				 * Use BufferedWriter's write() method when you simply want to print info.
				 * If want to format info as you're writing it to a stream, use the PrintWriter instead:
				 * 
				 * PrintWriter writer = new PrintWriter(char2byte);
				 * writer.printf("blah %,.2f", arg);
				 */
				
				//----------------And using PrintWriter--------------- NOTE: Doesn't throw exceptions!!! Have to manually check via writer.checkError(); !!!
//				PrintWriter writer = new PrintWriter(chars2bytes);
//				for (int i = 0; i < 5; i++) {
//					writer.printf("This is line number: %d%n", i);
//					
//					if(writer.checkError()){
//						System.err.println("ERROR: PrintWriter failed somewhere....");
//					}
//					
//				}//for
//				writer.close();
				
			} catch (IOException e) {
				//obviously have a better catch here.
				System.err.println("IO ERROR: Failed to write to file.");
			} 
				
			System.out.println("done writing to: " + file.getName() + ", now reading:");
			
		}//if-write
		
		if(file.isFile() && file.canRead()){
			//We can read from our file:
			
			//NOTE: reading can go in a method, simply pass in a file name to read
			try {
				
				FileReader fr = new FileReader(file);
				BufferedReader br = new BufferedReader(fr);
				String line = null;
				while( (line = br.readLine()) != null){
					System.out.println(line);
				}
				br.close();
				
				/*****************
				 * Note that there is no reader equivalent of PrintWriter.
				 */
				
			} catch (IOException e) {
				//obviously have a better catch here.
				System.err.println("IO ERROR: Failed to read file.");
			}
			
			System.out.println("done reading.");
		}//if-read
			
	}//main

}//class
