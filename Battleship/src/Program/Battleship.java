package Program;
import java.util.ArrayList;

/*
 * Class used to represent a battleship.
 * It implements logic that decides if a user hit or missed this ship. 
 * It is a scaled up version of the prototype in "SimpleBattleship"
 */

public class Battleship {

	//Instance variables (have default values)
	private ArrayList<String> locationCells; //ArrayList to allow removal of objects already guessed
	private String name;
	
	//Setters:
	public void setLocationCells(ArrayList<String> locations) {
		this.locationCells = locations;
	}
	
	public void setName(String bShipName){
		this.name = bShipName;
	}

	//Methods:
	public String checkYourself(String usrGuess) {
		
		String result = "miss";
		
		//First check if user's guess is in the ArrayList (Battleship's coordinates)
		if(locationCells.contains(usrGuess)){
			//valid coordinate guess:
			
			locationCells.remove(usrGuess); // remove it from the coordinate list
			
			if(locationCells.isEmpty()){
				//user guessed all coordinates of this BattleShip, the ship sinks.
				result = "kill";
				System.out.println("Ouch! You sunk " + name);
			} else {
				//user guessed a valid coordinate of this BattleShip, but has not yet sunk it.
				result = "hit";
			}
		}
		
		return result;
		
	}//checkYourself
}//class
