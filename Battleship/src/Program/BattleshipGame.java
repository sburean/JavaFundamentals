package Program;
import java.util.ArrayList;
//import TestCases.*;

/*
 * The class that runs the actual game. Functionality broken up into four (4) classes. 
 * This helps testing and debugging by keeping the granularity smaller. (Easier code modification & debugging). 
 * 
 * The final version of the game contains THREE battleships represented on a virtual GRID (7 x 7).
 *  - Grid starts at A0 on the bottom left hand corner.
 *  - The grid increments letters upwards (up to G), and numbers eastwards (up to 6) 
 */

public class BattleshipGame {
	
	//Instance variables:
	private ArrayList<Battleship> ships = new ArrayList<>(); 
	private static GameHelper helper = new GameHelper();
	private int numOfGuesses = 0;
	
	public static void main(String[] args) {
		/*
		 * Sets up the game and starts it. 
		 */
		
//		GameHelperTest helperTest = new GameHelperTest();
//		helperTest.testGenerateCoords();
	
		BattleshipGame game = new BattleshipGame();
		game.setUpGame();
		game.startPlaying();
		
	}//main
	
	/**
	 * Initialize THREE battleships, set names and place them into the "ships" ArrayList.<br>
	 * Assigns valid coordinates to each battleship via the helper object.
	 */
	public void setUpGame(){

		//Create Battleships:
		Battleship bShip1 = new Battleship();
		Battleship bShip2 = new Battleship();
		Battleship bShip3 = new Battleship();
		
		//Name each Battleship:
		bShip1.setName("Ship1");
		bShip2.setName("Ship2");
		bShip3.setName("Ship3");
		
		//Add each Battleship to the "ships" ArrayList:
		ships.add(bShip1);
		ships.add(bShip2);
		ships.add(bShip3);
		
		System.out.println("Your goal is to sink three battleships:");
		System.out.println("Try to sink them all in the fewest number of guesses!");
		
		//For each battleship in "ships", assign valid coordinates via helper object.
		for (Battleship battleship : ships) {
			battleship.setLocationCells(helper.generateCoords(3));
		}
		
	}//setUpGame
	
	/**
	 * While there is a battleship left standing:<br>
	 * <BLOCKQUOTE>- Asks for player guesses and checks each guess.<br>
	 * - Displays information to the user if they hit, miss, or sunk a battleship.</BLOCKQUOTE>
	 */
	public void startPlaying(){
		
		while(!ships.isEmpty()){
			String usrInput = helper.getUsrInput("Enter a guess ");
			checkUserGuesses(usrInput);
		}
		finishGame();
		
	}//startPlaying
	
	/**
	 * Loops through all battleships and calls each one's "checkYourself" method against player's guess.
	 */
	public void checkUserGuesses(String usrGuess){
		
		numOfGuesses++;
		String result = "miss";
		for (Battleship battleship : ships) {
			result = battleship.checkYourself(usrGuess); 
			if(result.equals("hit")){
				//User guessed a correct coordinate and hit a battleship:
				break;
			} else if(result.equals("kill")){
				//User guessed a correct coordinate and sunk a battleship:
				ships.remove(battleship);
				break;
			}
		}
		System.out.println(result);
		
	}//checkUserGuesses
	
	/**
	 * Prints message to user based on their performance (number of guesses).
	 */
	public void finishGame(){
		
		System.out.println("Game Over!");
		if(numOfGuesses < 18) {
			System.out.println("Grrrreatt! It only took you " + numOfGuesses + " tries!");
		} else {
			System.out.println("Coolio, it took you " + numOfGuesses + " tries.");
		}
		
	}//finishGame
	
}//class
