package Program;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/*
 * A helper class that's used to:
 * - Get user input.
 * - Generate the 7x7 GRID.
 */
public class GameHelper {
	
	private static final String alphabet = "abcdefg"; 
	private int gridLength = 7;
	private int gridSize = 49;
	private int [] grid = new int[gridSize]; 
	private int comCount = 0;
	
	/**
	 * Prompts the user to enter a guess for a coordinate value. 
	 * 
	 * @param promptMsg Message displayed to user.
	 * @return A string containing user's guess at a coordinate.
	 */
	public String getUsrInput(String promptMsg){
		
		System.out.print(promptMsg + " ");
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		String usrInput = null;
		
		try {
			//Catch any java.io exceptions
			usrInput = input.readLine();
			if (usrInput.length() == 0) return null; //user didn't actually type anything
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return usrInput;
		
	}//getUsrInput
	
	/**
	 * Generates random coordinate values for a Battleship on a 7x7 grid.<br>
	 * Coordinates will be either horizontal or vertical, and a sequence of three values.<br>
	 * (ie: [A0, A1, A2] - horizontal, or [B3, C3, D3] - vertical)<br>
	 * 
	 * @return A String-ArrayList containing triple, sequential coordinate values. 
	 */
	public ArrayList<String> generateCoords(int comSize){
		
//		/* My version:
//		 * NOTE: Currently hard-coded to generate THREE coordinates for EACH battleship. 
//		 * 			This can be changed if needed (take a parameter to specify).
//		 * ALSO, this doesn't ensure that two ships aren't generated on-top of eachother....
//		 */
//		
//		int orientation = (int) (Math.random()*2);
//		char letterStart = (char) (97 + (int) (Math.random() * 5) ); //97 = ASCII for 'a'. Total range is 7, [a:g], but want at least two consecutive spaces after starting point.
//		int numberStart = (int) (Math.random() * 5 ); // max range is 7 [0:6], but want at least two consecutive values after starting point.
//		
//		if(orientation == 1) {
//			
//			//1 means horizontal placement (letter constant, numbers increment)
//			String letter = Character.toString(letterStart);
//			ArrayList<String> coords = new ArrayList<>();
//			coords.add(letter + Integer.toString(numberStart));
//			coords.add(letter + Integer.toString(numberStart+1));
//			coords.add(letter + Integer.toString(numberStart+2));
//			return coords;
//			
//		} else {
//			
//			//0 means vertical placement (number constant, letters increment)
//			String number = Integer.toString(numberStart);
//			ArrayList<String> coords = new ArrayList<>();
//			coords.add(Character.toString(letterStart) + number);
//			coords.add(Character.toString((char)(letterStart+1)) + number);
//			coords.add(Character.toString((char)(letterStart+2)) + number);
//			return coords;
//			
//		}
		
		ArrayList<String> alphaCells = new ArrayList<String>();
//		String [] alphacoords = new String [comSize]; 
		String temp = null;
		int [] coords = new int[comSize];
		int attempts = 0;
		boolean success = false; 
		int location = 0;
		
		comCount++;
		int incr = 1;
		if ((comCount % 2) == 1) {
			incr = gridLength;
		}
		
		//Main search loop to get starting point
		while ( !success & attempts++ < 200 ) { 
			location = (int) (Math.random() * gridSize);
			//System.out.print(“ try “ + location);
			int x = 0;
			success = true;
			while (success && x < comSize) {
				if (grid[location] == 0) {
					coords[x++] = location;
					location += incr;
					if (location >= gridSize) {
						success = false;
					}
					if (x>0 && (location % gridLength == 0)) {
						success = false;
					}
				} else {
					// System.out.print(“ used “ + location);
					success = false;
				}
			}//while
		}//while
		
		//turn each location into alpha-coords:
		int x = 0;
		int row = 0;
		int column = 0;
		// System.out.println(“\n”); 
		while (x < comSize) {
			grid[coords[x]] = 1;
			row = (int) (coords[x] / gridLength);
			column = coords[x] % gridLength;
			temp = String.valueOf(alphabet.charAt(column));
			alphaCells.add(temp.concat(Integer.toString(row)));
			x++;
//			 System.out.print(" coord "+x+" = " + alphaCells.get(x-1)); //gives coordinate locations
		}
		// System.out.println(“\n”);
		return alphaCells;
		
	}//generateCoords

}
