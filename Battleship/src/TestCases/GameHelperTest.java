package TestCases;

import java.util.ArrayList;
import Program.GameHelper;

/**
 * Test-cases for each method in the GameHelper class.
 */
public class GameHelperTest {

	/**
	 * Since this is randomly generated, there is no set values to check for.<br>
	 * Will have to make sure displayed values are valid.<br>
	 * NOTE: Can write a REGEX on the printed string to ensure it contains valid characters, but too much work for an example program.
	 */
	public void testGenerateCoords(){

		//NOTE: This is for my version of "generateCoords" only. 
		
//		GameHelper helper = new GameHelper();
//		ArrayList<String> coords = helper.generateCoords();
//		
//		System.out.print("[ ");
//		for (String string : coords) {
//			System.out.print(string + " ");
//		}
//		System.out.print("]");
		
	}
	
}
