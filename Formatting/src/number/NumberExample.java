package number;

public class NumberExample {

	public static void main(String[] args) {

		//Can use String.format(..) to format a string:
		String s = String.format("%-12s%-12s%-12s\n","Column 1","Column 2","Column 3");
		
		//And then print it using println/print:
        System.out.println(s);
        
        //OR, can directly print a FORMATTED string using printf:
        System.out.printf("%8d%12d%12d\n",15,12,53434);
        
        //Now we can simply do this: (printf calls format internally)
        System.out.format("%8d%12d%12d\n",15,12,53434);
        
        
        //==============================================================================
        
        //Valid arguments for %d (expecting a decimal) due to IMPLICIT PROMOTION
        byte i1 = 10;
        short i2 = 10;
        int i3 = 10;
        
        //Valid arguments for %d (expecting a decimal) due to AUTOBOXING
        Integer i4 = new Integer(10);
        
        //Test
        System.out.printf("\n%d",i1);
        
        //==============================================================================
        
        
		
	}

}
