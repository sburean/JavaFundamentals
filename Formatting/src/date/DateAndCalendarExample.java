package date;

import java.util.Calendar;
import java.util.Date;

public class DateAndCalendarExample {

	public static void main(String[] args) {
		
		//Formatting dates have two-character type; starting with 't': (* See online docs for full details.)
		String s = String.format("%tA, %<tB %<td", new Date()); // '<' means to use the previous argument again
		System.out.println(s);
		
		/*
		 * Use java.util.Date class when you want to get a TIMESTAMP of NOW!
		 * (or quickly converting dates <--> millisecond representation) 
		 */
		
		Date date = new Date();
		System.out.println(date);
		System.out.printf("%tc", date); // c = complete details of a Date object as it appears by new Date().
		System.out.println();	
		
		/*
		 * For any actual date manipulation, use java.util.Calendar.
		 * Ask for a concreteCalendar instance from Calendar class via the static "getInstance()" method.
		 */
		
		Calendar calendar1 = Calendar.getInstance();
		
		/*
		 * NOTE: Concrete calendar returned is BASED ON LOCALE! 
		 * 			(Most-likely the Gregorian calendar: java.util.GregorianCalendar)
		 * 			-> Can get other libraries: java.util.BuddhistCalendar, etc...
		 * 
		 * ALL concrete calendars are sub-types of java.util.Calendar, 
		 * and can polymorphically assign to reference variable of type Calendar. 
		 */
		
		calendar1.set(2016, 10, 6); 
		
		/*
		 * Nov 6th, 2016: 
		 * - 2nd WEEK_OF_MONTH - it is the second week of the month ranging from [Sunday-Saturday]
		 * - the 6th is a day in the 1st numerical week of the month ranging from (1-31): "DAY_OF_WEEK_IN_MONTH" 
		 */
		
		//By default, new calendar objects are initialized with the CURRENT time-stamp. (Time is in millis from epoch)
		System.out.println("\nWEEK_OF_MONTH:" + calendar1.get(Calendar.WEEK_OF_MONTH));
		System.out.println("DAY_OF_WEEK_IN_MONTH:" + calendar1.get(Calendar.DAY_OF_WEEK_IN_MONTH));
		System.out.println();
		
		//Some operations on a calendar:
		long c1Millis = calendar1.getTimeInMillis(); // save calendar1's time as millisecond representation
		System.out.printf("Calendar1's time in millis: %35d%n", c1Millis);
		try {
			Thread.sleep(10); //delay 10 millis (not exact)
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		
		Calendar calendar2 = Calendar.getInstance(); //New calendar; default values = NOW time-stamp.
		System.out.printf("Calendar2's time in millis: (when initialized) %16d%n", calendar2.getTimeInMillis());
		calendar2.setTimeInMillis(c1Millis); //set calendar2's time value to calendar1's 
		System.out.printf("Calendar2's time in millis: (after set = c1) %18d%n", calendar2.getTimeInMillis());
		
		//Printing today's date:
		Calendar today = Calendar.getInstance();
		System.out.printf("\nToday is: (formatted) %12tA, %<tB %<tdth %<tY at %<tr.%n", today);
		//OR THE NON-FORMATTED VERSION:
		System.out.println("Today is: (not formatted) " + today.getTime() + ".");
		
	}

}
