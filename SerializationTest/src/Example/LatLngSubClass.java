package Example;

import java.io.Serializable;

/* If we have a non-serializable class, we can extend it and mark the subclass as serializable.
 * (As we're doing here)
 * 
 * A class should only be made serializable if it has access to 
 * the no-arg constructor of its super-class! 
 * (Will be used in deserialization process to re-initialize non-serializable super-classes)
 * 
 * If a no-arg constructor is NOT available, as in this case, will have to mark the
 * reference to that object as transient, and step into the serialization process to
 * serialize that object's data fields and then re-initialize it during deserialization. 
 * (Instead of sub-classing it -> so this class would not be used)
 */
public class LatLngSubClass extends LatLngTemp implements Serializable{

	public LatLngSubClass(double latitude, double longtitude) {
		super(latitude, longtitude);
	}

	private static final long serialVersionUID = 1L; //serialVersionUID constant
	
}
