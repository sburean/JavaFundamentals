package Example;

/**
 * Non-serializable super-class; subclass depends on its state (Lat & Lng values)
 * No access to this class, part of google API
 *
 */
public class LatLngTemp {
	
	private final double lat, lng;
	
	public LatLngTemp(double latitude, double longtitude) {
		this.lat = latitude;
		this.lng = longtitude;
	}
	
	public double getLat(){
		return this.lat;
	}
	
	public double getLng(){
		return this.lng;
	}

	@Override
	public String toString() {
		return getLat() + ", " + getLng();
	}
	
}
