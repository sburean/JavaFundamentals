package Example;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class TestStub {

	public static void main(String[] args) {
	
		//Create LatLngUse
		LatLngUse user1 = new LatLngUse();
		
		//test:
		user1.printData();
		System.out.println("hash before (based on mem. address): "+ user1.hashCode());
		
		//Serialize:
		try {
			
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(new File("test.txt")));
			out.writeObject(user1);
			System.out.println("============SERIALIZING DONE===========");
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Deserialize:
		LatLngUse user1restored = null;
		try {
			
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(new File("test.txt")));
			user1restored = (LatLngUse) in.readObject();
			System.out.println("===========DESERIALIZING DONE==========");
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//test again:
		assert user1restored != null; //reference not pointing to nothing.
		user1restored.printData();
		System.out.println("hash after (based on mem. address): "+ user1restored.hashCode());
		
	}
	
}
