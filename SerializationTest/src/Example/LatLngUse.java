package Example;

import java.io.IOException;
import java.io.Serializable;

public class LatLngUse implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private transient LatLngTemp latlng = new LatLngTemp(11.11, 22.22); //using LatLngTemp
//	private transient LatLngTemp latlng = new LatLngSubClass(33.33, 44.44);//using LatLngSubClass

	/*
	 * Instance variable is marked as transient because LatLngSubClass does NOT have access
	 * to the no-arg constructor of its first non-serializable superclass: LatLngTemp... it doens't exist.
	 * 
	 * So we mark it as transient to skip over it, and if we need data from it, we have to
	 * step into the serialization/deserialization process and save its data fields, and
	 * create A NEW LatLngTemp object and restore its data fields ourselves:
	 */
	
	public void printData(){
		System.out.println(latlng);
	}
	
	//Step into Serialization:
	private void writeObject(java.io.ObjectOutputStream out) throws IOException {
		out.defaultWriteObject(); //perform default serialization writes
		
		//now write SERIALIZABLE data from the transient field:
		out.writeDouble(latlng.getLat());
		out.writeDouble(latlng.getLng());
	}
	
	 private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
		 in.defaultReadObject(); //perform default deserialization 
		 
		 //now read data back IN THE SAME ORDER as written in writeObject
		 Double newLat = in.readDouble();
		 Double newLng = in.readDouble();
//		 latlng = new LatLngSubClass(newLat, newLng);
		 latlng = new LatLngTemp(newLat, newLng);
	 }

}
