package Code;
/*
 * The non-serializable superclass. 
 */
public class SuperClass1 {

	protected int i;
	
	public SuperClass1() {
		i = 1;
	}
	
	public void incI(){
		i++;
	}
	
	public int getI(){
		return this.i;
	}
	
	public String print(){
		return String.valueOf(i);
	}
	
}
