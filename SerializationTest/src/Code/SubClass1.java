package Code;
/*
 * When deserializing an object, if any object (in the object graph) 
 * has a non-serializable superclass, it's constructor will run, along with ALL 
 * of the constructors above the non-serializable superclass (regardless if they're serializable or not).
 * Can't stop constructor chaining once it starts.
 * 
 * This tests that.
 * NOTE: only one object in this object graph; object "sc1" - see testStub.
 */

@SuppressWarnings("serial")
public class SubClass1 extends SuperClass1 implements java.io.Serializable {

	private int j;
	
	public SubClass1() {
		j = 1;
	}
	
	public void incJ(){
		j+=getI();
	}
	
	public int getJ(){
		return this.j;
	}
	
	public String print(){
		return String.valueOf(j);
	}
	
	public String printSuper(){
		return String.valueOf(getI());
	}
	
}
