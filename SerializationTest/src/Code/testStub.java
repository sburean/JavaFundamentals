package Code;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/*
 * TLDR: Be careful when serializing/deserializing objects that 
 * 	depend on a non-serializable superclass member within their inheritance tree. 
 */

public class testStub {

	public static void main(String[] args) {
		
		/*
		 * create object of type subclass1.
		 * runs constructor and superclass1's constructor via chaining.
		 * expect i = 1 and j = 1
		 */
		SubClass1 sc1 = new SubClass1();

		System.out.println("i from SubClass1 = " + sc1.printSuper() + " as default");
		System.out.println("j from SubClass1 = " + sc1.print() + " as default");
		
		sc1.incI(); // change state of non-serializable superclass. (THIS AFFECTS incJ() METHOD!)
		sc1.incJ(); // change state of this serializable class.
		
		System.out.println("i from SubClass1 = " + sc1.printSuper() + " after increment");
		System.out.println("j from SubClass1 = " + sc1.print() + " after increment");
		
		//serialize:
		try{
			FileOutputStream fs = new FileOutputStream("test.tmp");
			ObjectOutputStream oos = new ObjectOutputStream(fs);
			oos.writeObject(sc1);
			oos.close();
		} catch (Exception e){
			e.printStackTrace();
		}
		
		//deserialize:
		
		
		SubClass1 sc1Deserialized = null;
		try {
			FileInputStream is = new FileInputStream("test.tmp");
			ObjectInputStream ois = new ObjectInputStream(is);
			sc1Deserialized = (SubClass1) ois.readObject();
			ois.close();
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
		} catch (Exception e){
			System.err.println("Exception block");
		}

		/*
		 * We expect to see :
		 * i = 1 (this was not restored to it's previous value of 2, we RESTORED object: sc1)
		 * j = 3
		 * after deserialization of sc1 (simply restoring state of subclass1 object)
		 */
		
		System.out.println("i from SubClass1 = " + sc1Deserialized.printSuper() + " after deserialization");
		System.out.println("j from SubClass1 = " + sc1Deserialized.print() + " after deserialization");
		
		/*
		 * So when we increase I and J AGAIN, we expect to see:
		 * i = 2
		 * j = 5
		 * 
		 *  
		 * This is correct, however it's not consistent with what we would expect from our OLD state
		 * where the old value of i = 2 before the second value increase, in which case we would get: ( after another increase: incI() & incJ() )
		 * i = 3
		 * j = 6
		 * 
		 * THIS OCCURS BECAUSE NON-SERIALIZABLE SUPER-CLASSES ARE CREATED FROM SCRATCH, AND THEIR CONSTRUCTOR RUNS!
		 */
		
		sc1Deserialized.incI();
		sc1Deserialized.incJ();
		
		System.out.println("i from SubClass1 = " + sc1Deserialized.printSuper() + " after deserialization + second increment");
		System.out.println("j from SubClass1 = " + sc1Deserialized.print() + " after deserialization + second increment");
		
		
	}

}
